set(HIVE_TRACKER_SRCS main.cpp Singleton.cpp)

include_directories(${CMAKE_CURRENT_BINARY_DIR})

qt6_add_resources(HIVE_TRACKER_DATA_SRC qml.qrc)

configure_file(HiveTrackerDB/TypesDefinitions.h ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/TypesDefinitions.h COPYONLY)

set(HTDB_GENERATED_SRC
  ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/Apiary.cpp
  ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/Box.cpp
  ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/Database.cpp
  ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/Feeding.cpp
  ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/FeederInspection.cpp
  ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/Hive.cpp
  ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/Inspection.cpp
  ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/Journal.cpp
  ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/JournalEntry.cpp
  ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/Queen.cpp
  ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/Qml.cpp
  ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/Picture.cpp
  ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/Treatment.cpp
  ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB/HiveActivity.cpp
  )

add_custom_command(OUTPUT ${HTDB_GENERATED_SRC}
                   DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/HiveTrackerDB.ard ${PARC_PROGRAM}
                   COMMAND ${PARC_PROGRAM} cyqlops-db-sqlite-qml-cpp sqlite ${CMAKE_CURRENT_SOURCE_DIR}/HiveTrackerDB.ard ${CMAKE_CURRENT_BINARY_DIR}/HiveTrackerDB )

add_executable(HiveTracker ${HIVE_TRACKER_SRCS} ${HIVE_TRACKER_DATA_SRC} ${HTDB_GENERATED_SRC})
target_link_libraries(HiveTracker Qt6::Quick Cyqlops::DB::SQLite)
