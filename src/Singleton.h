#pragma once

#include <QObject>

#include <HiveTrackerDB/Inspection.h>

namespace HiveTracker
{

  class Singleton : public QObject
  {
    Q_OBJECT
  public:
    Singleton(QObject* _parent = nullptr);
    ~Singleton();
    Q_INVOKABLE QByteArray createUuid();
    Q_INVOKABLE HiveTrackerDB::Apiary apiary(HiveTrackerDB::Apiary* _apiary);
    Q_INVOKABLE HiveTrackerDB::Apiary* apiaryPtr(const HiveTrackerDB::Apiary& _apiary);
    Q_INVOKABLE HiveTrackerDB::Hive hive(HiveTrackerDB::Hive* _hive);
    Q_INVOKABLE HiveTrackerDB::Hive* hivePtr(const HiveTrackerDB::Hive& _hive);
    Q_INVOKABLE HiveTrackerDB::Inspection* inspectionPtr(const HiveTrackerDB::Inspection& _apiary);

    Q_INVOKABLE ::HiveTrackerDB::Inspection::BroodFlags broodFlags(int _value);
    Q_INVOKABLE ::HiveTrackerDB::Inspection::BehaviorFlags behaviorFlags(int _value);
    Q_INVOKABLE ::HiveTrackerDB::Inspection::HealthFlags healthFlags(int _value);
  };
}
