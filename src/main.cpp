#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>

#include <Cyqlops/DB/SQLite/Database.h>
#include <HiveTrackerDB/Database.h>
#include <HiveTrackerDB/Qml.h>

#include "Singleton.h"

int main(int argc, char *argv[])
{
  QGuiApplication::setApplicationName("HiveTracker");
  QGuiApplication app(argc, argv);
  
  qmlRegisterSingletonType<HiveTracker::Singleton>("HiveTracker", 1, 0, "HiveTracker", [](QQmlEngine *, QJSEngine *) -> QObject* { return new HiveTracker::Singleton; });

  HiveTrackerDB::Qml::initialise();

  Cyqlops::DB::SQLite::Database db;
  QString db_filename = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/HiveTracker.db";

  QDir().mkpath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
  
  qDebug() << "Database filename: " << db_filename;

  db.enableChangeMonitoring();
  db.setFilename(db_filename);
  HiveTrackerDB::Database* ht_db = new HiveTrackerDB::Database(&db);
  ht_db->setNotificationsEnabled(true);

  QQmlApplicationEngine engine;
  engine.addImportPath("qrc:/qml");

  engine.rootContext()->setContextProperty("HTDBINSTANCE", ht_db);
  
  engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

  return app.exec();
}

