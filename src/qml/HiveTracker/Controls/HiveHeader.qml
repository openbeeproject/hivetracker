import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15

import HiveTracker 1.0
import HiveTrackerDB 1.0 as HTDB

Pane
{
  id: _root_
  property var hive
  signal closeView()
  property alias showBackButton: _back_button_.visible
  GridLayout
  {
    id: _label_hive_name_layout_
    columns: 2
    Label
    {
      text: hive ? hive.name : ""
      font.pointSize: 20
      font.bold: true
      Layout.alignment: Qt.AlignHCenter
    }
    Image
    {
      source: "qrc:/data/hive.png"
      Layout.rowSpan: 2
      Layout.preferredWidth: 32
      Layout.preferredHeight: 32
      Layout.alignment: Qt.AlignVCenter
    }
    Label
    {
      text: hive ? HiveTracker.apiaryPtr(hive.apiary).name : ""
      font.pointSize: 10
      Layout.alignment: Qt.AlignHCenter
      Material.foreground:  Material.color(Material.Grey, Material.Shade900)
    }
    anchors.centerIn: parent
  }
  RoundButton
  {
    id: _back_button_
    icon.source: "qrc:/data/backward.png"
    onClicked: _root_.closeView()
    anchors.left: parent.left
    anchors.verticalCenter: parent.verticalCenter
    Material.background: Material.Orange
  }
  Material.background: Material.color(Material.Orange, Material.Shade300)
  Layout.fillWidth: true
  Layout.preferredHeight: Math.max(_label_hive_name_layout_.height, _back_button_.height) + 10
}