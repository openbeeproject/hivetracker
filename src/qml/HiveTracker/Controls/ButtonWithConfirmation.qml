import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

RoundButton
{
  id: _root_
  onClicked: _confirm_.visible = !_confirm_.visible

  signal confirmed()
  function hideConfirmation()
  {
    _confirm_.visible = false
  }
  RoundButton
  {
    id: _confirm_
    Material.background: Material.Green
    icon.source: "qrc:/data/done.png"
    anchors.right: _root_.right
    anchors.bottom: _root_.top
    visible: false
    onClicked: {
      _root_.confirmed()
      visible = false
    }
  }

}