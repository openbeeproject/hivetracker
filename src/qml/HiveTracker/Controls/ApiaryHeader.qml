import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15

Pane
{
  id: _root_
  property var apiary
  signal closeView()
  RowLayout
  {
    id: _label_apiary_name_layout_
    Label
    {
      text: apiary ? apiary.name : ""
      font.pointSize: 20
      font.bold: true
    }
    Image
    {
      source: "qrc:/data/apiary.png"
      Layout.preferredWidth: 32
      Layout.preferredHeight: 32
      Layout.alignment: Qt.AlignVCenter
    }
    anchors.centerIn: parent
  }
  RoundButton
  {
    id: _back_button_
    icon.source: "qrc:/data/backward.png"
    onClicked: _root_.closeView()
    anchors.left: parent.left
    anchors.verticalCenter: parent.verticalCenter
    Material.background: Material.Orange
  }
  Material.background: Material.color(Material.Orange, Material.Shade300)
  Layout.fillWidth: true
  Layout.preferredHeight: Math.max(_label_apiary_name_layout_.height, _back_button_.height) + 4
}
