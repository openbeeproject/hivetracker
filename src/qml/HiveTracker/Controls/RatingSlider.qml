import QtQuick 2.15
import QtQuick.Controls 2.15

Slider
{
  id: _root_
  value: -1
  from: -1
  to: 10
  stepSize: 1
  background: Row
  {
    Rectangle
    {
      color: "gray"
      height: _root_.height
      width: height
    }
    Rectangle
    {
      height: _root_.height
      width: _root_.width - height
      gradient: Gradient
      {
        orientation: Gradient.Horizontal
        GradientStop { position: 0.0; color: "red" }
        GradientStop { position: 0.45; color: "yellow" }
        GradientStop { position: 0.55; color: "yellow" }
        GradientStop { position: 1.0; color: "green" }
      }
    }
  }
}