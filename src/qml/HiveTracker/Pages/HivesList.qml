import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15

import HiveTracker.Controls 1.0

import HiveTrackerDB 1.0 as HTDB
import HiveTrackerDB.Hive 1.0 as HTDBHive

ColumnLayout
{
  id: _root_
  property HTDB.Database database
  property var apiary
  property var selectedHive
  onSelectedHiveChanged:
  {
    _hive_delete_.hideConfirmation()
  }

  ApiaryHeader
  {
    apiary: _root_.apiary
    onCloseView: _root_.StackView.view.pop()
  }
  Item
  {
    HTDBHive.SelectQuery
    {
      id: _select_hives_
      database: _root_.database
      autoExecute: true
      HTDBHive.ByApiary
      {
        apiary: _root_.apiary ? _root_.apiary.id : 0
      }
    }
    ListView
    {
      id: _list_view_
      model: _select_hives_.model
      anchors.fill: parent
      delegate: Pane
      {
        RowLayout
        {
          id: _layout_
          width: _list_view_.width
          Image
          {
            source: "qrc:/data/hive.png"
            Layout.preferredWidth: 32
            Layout.preferredHeight: 32
          }
          Label
          {
            id: _label_name_
            text: modelData.name
            Layout.fillWidth: true
          }
        }
        MouseArea
        {
          anchors.fill: parent
          onClicked:
          {
            if(_root_.selectedHive == modelData)
            {
              _root_.StackView.view.push("Hive.qml", {"database": _root_.database, "hive": _root_.selectedHive})
            }
            _root_.selectedHive = modelData
          }
        }
        contentHeight: _layout_.height
        contentWidth: parent.width
        Material.background: _root_.selectedHive == modelData ? Material.Yellow : Material.White
      }
    }
    RowLayout
    {
      ButtonWithConfirmation
      {
        id: _hive_delete_
        icon.source: "qrc:/data/delete.png"
        enabled: _root_.selectedHive != null
        onConfirmed: {
          _root_.selectedHive.erase()
          _root_.selectedHive = null
        }
        Material.background: Material.Red
      }
      RoundButton
      {
        Material.background: Material.Yellow
        enabled: _root_.selectedHive != null
        icon.color: "transparent"
        icon.source: "qrc:/data/edit.png"
        onClicked: _root_.StackView.view.push("HiveEditor.qml", {"hive": _root_.selectedHive, "database": _root_.database})
      }
      RoundButton
      {
        Material.background: Material.Yellow
        icon.color: "transparent"
        icon.source: "qrc:/data/hive_plus.png"
        onClicked: _root_.StackView.view.push("HiveEditor.qml", {"database": _root_.database, "apiary": _root_.apiary})
      }
      anchors.right: parent.right
      anchors.bottom: parent.bottom
    }
    Layout.fillWidth: true
    Layout.fillHeight: true
  }
}
