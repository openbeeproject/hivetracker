import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15

import HiveTracker.Controls 1.0

import HiveTrackerDB 1.0 as HTDB
import HiveTrackerDB.Apiary 1.0 as HTDBApiary

Item
{
  id: _root_
  property HTDB.Database database

  HTDBApiary.SelectQuery
  {
    id: _selectApiaries_
    database: _root_.database
    autoExecute: true
  }
  property var selectedApiary
  onSelectedApiary:
  {
    _apiary_delete_.hideConfirmation()
  }
  ListView
  {
    id: _list_view_
    model: _selectApiaries_.model
    anchors.fill: parent
    delegate: Pane
    {
      ColumnLayout
      {
        id: _layout_
        width: _list_view_.width
        Label
        {
          id: _label_name_
          text: modelData.name
        }
        Label
        {
          text: modelData.address
          font.pointSize: _label_name_.font.pointSize * 0.8
          Layout.alignment: Qt.AlignRight
          Material.foreground: Material.Grey
        }
      }
      MouseArea
      {
        anchors.fill: parent
        onClicked:
        {
          if(_root_.selectedApiary == modelData)
          {
            _root_.StackView.view.push("HivesList.qml", {"apiary": _apiaries_list_.selectedApiary, "database": _root_.database})
          }
          _root_.selectedApiary = modelData
        }
      }
      contentHeight: _layout_.height
      contentWidth: parent.width
      Material.background: _root_.selectedApiary == modelData ? Material.Yellow : Material.White
    }
  }
  RowLayout
  {
    ButtonWithConfirmation
    {
      id: _apiary_delete_
      icon.source: "qrc:/data/delete.png"
      enabled: _root_.selectedApiary != null
      onConfirmed: {
        _root_.selectedApiary.erase()
        _root_.selectedApiary = null
      }
      Material.background: Material.Red
    }
    RoundButton
    {
      Material.background: Material.Yellow
      enabled: _root_.selectedApiary != null
      icon.color: "transparent"
      icon.source: "qrc:/data/edit.png"
      onClicked: _root_.StackView.view.push("ApiaryEditor.qml", {"apiary": _apiaries_list_.selectedApiary, "database": _root_.database})
    }
    RoundButton
    {
      Material.background: Material.Yellow
      icon.color: "transparent"
      icon.source: "qrc:/data/apiary_plus.png"
      onClicked: _root_.StackView.view.push("ApiaryEditor.qml", {"database": _root_.database})
    }
    anchors.right: parent.right
    anchors.bottom: parent.bottom
  }
}