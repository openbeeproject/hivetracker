import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

import HiveTracker 1.0
import HiveTracker.Controls 1.0
import HiveTrackerDB 1.0 as HTDB
import Cyqlops.Controls 1.0

Item
{
  id: _root_
  property HTDB.Database database
  property var hive
  property var inspection
  property bool __showExtendedBrood: _brood_slider_.value >= 0 && _brood_slider_.value <= 5
  property bool __showExtendedBehavior: _behavior_slider_.value >= 0 && _behavior_slider_.value <= 5
  property bool __showExtendedHealth: _health_slider_.value >= 0 && _health_slider_.value <= 5

  function __set_details(details, widgets, values)
  {
    if(widgets.length != values.length)
    {
      console.log(widgets, values, " don't have the same length")
    }
    console.log(details)
    for(var i = 0; i < Math.min(widgets.length, values.length); ++i)
    {
      console.log(i, details, values[i], ((details & values[i]) != 0))
      widgets[i].checked = ((details & values[i]) != 0)
    }
  }
  onInspectionChanged:
  {
    if(inspection)
    {
      __set_details(inspection.broodDetails, [_eggs_, _larvae_, _capped_brood_], [HTDB.Inspection.BroodFlags.Eggs, HTDB.Inspection.BroodFlags.Larva, HTDB.Inspection.BroodFlags.CappedBrood])
      __set_details(inspection.behaviorDetails, [_aggressive_, _swarming_, _weak_], [HTDB.Inspection.BehaviorFlags.Aggressive, HTDB.Inspection.BehaviorFlags.Swarming, HTDB.Inspection.BehaviorFlags.Weak])
      __set_details(inspection.healthDetails,
            [_other_, _queenless_, _dead_colony_, _varroa_, _hive_beetles_, _funky_smell_, _bald_brood_, _wax_moths_, _deformed_wings_, _nosema_, _american_fool_brood_, _european_fool_brood_],
            [HTDB.Inspection.HealthFlags.Other, HTDB.Inspection.HealthFlags.Queenless, HTDB.Inspection.HealthFlags.DeadColony, HTDB.Inspection.HealthFlags.Varroa, HTDB.Inspection.HealthFlags.HiveBeetles, HTDB.Inspection.HealthFlags.FunkySmell, HTDB.Inspection.HealthFlags.BaldBrood, HTDB.Inspection.HealthFlags.WaxMoths, HTDB.Inspection.HealthFlags.DeformedWings, HTDB.Inspection.HealthFlags.Nosema, HTDB.Inspection.HealthFlags.AmericanFoolBrood, HTDB.Inspection.HealthFlags.EuropeanFoolBrood ])
      _brood_slider_.value = inspection.brood
      _food_slider_.value = inspection.food
      _behavior_slider_.value = inspection.behavior
      _health_slider_.value = inspection.health
      _comments_.text = inspection.comments
    }
  }
  GridLayout
  {
    id: _grid_layout_
    anchors.fill: parent
    columns: 2
    HiveHeader
    {
      hive: _root_.hive
      showBackButton: false
      Layout.columnSpan: 2
      Layout.fillWidth: true
    }
    Label
    {
      text: "Date"
    }
    DateTimeField
    {
      id: _date_time_field_
      Layout.fillWidth: true
    }
    Label
    {
      text: "Brood"
    }
    RatingSlider
    {
      id: _brood_slider_
      Layout.fillWidth: true
    }
    Flow
    {
      visible: _root_.__showExtendedBrood
      CheckBox
      {
        id: _eggs_
        text: "Eggs?"
      }
      CheckBox
      {
        id: _larvae_
        text: "Larva?"
      }
      CheckBox
      {
        id: _capped_brood_
        text: "Capped brood?"
      }
      Layout.fillWidth: true
      Layout.columnSpan: 2
    }
    Label
    {
      text: "Food"
    }
    RatingSlider
    {
      id: _food_slider_
      Layout.fillWidth: true
    }
    Label
    {
      text: "Behavior"
    }
    RatingSlider
    {
      id: _behavior_slider_
      Layout.fillWidth: true
    }
    Flow
    {
      visible: _root_.__showExtendedBehavior
      CheckBox
      {
        id: _aggressive_
        text: "Aggressive"
      }
      CheckBox
      {
        id: _swarming_
        text: "Swarming"
      }
      CheckBox
      {
        id: _weak_
        text: "Weak"
      }
      Layout.columnSpan: 2
      Layout.fillWidth: true
    }
    Label
    {
      text: "Health"
    }
    RatingSlider
    {
      id: _health_slider_
      Layout.fillWidth: true
    }
    Flow
    {
      visible: _root_.__showExtendedHealth
      CheckBox
      {
        id: _queenless_
        text: "Queenlesss"
      }
      CheckBox
      {
        id: _dead_colony_
        text: "Dead Colony"
      }
      CheckBox
      {
        id: _varroa_
        text: "Varroa"
      }
      CheckBox
      {
        id: _hive_beetles_
        text: "Hive beetles"
      }
      CheckBox
      {
        id: _funky_smell_
        text: "Funky Smell"
      }
      CheckBox
      {
        id: _bald_brood_
        text: "Bald Brood"
      }
      CheckBox
      {
        id: _wax_moths_
        text: "Wax Moths"
      }
      CheckBox
      {
        id: _deformed_wings_
        text: "Deformed Wings"
      }
      CheckBox
      {
        id: _nosema_
        text: "Nosema"
      }
      CheckBox
      {
        id: _american_fool_brood_
        text: "American Fool Brood"
      }
      CheckBox
      {
        id: _european_fool_brood_
        text: "European Fool Brood"
      }
      CheckBox
      {
        id: _other_
        text: "Other"
      }
      Layout.columnSpan: 2
      Layout.fillWidth: true
    }
    Label
    {
      text: "Comments"
    }
    TextArea {
      id: _comments_
      Layout.fillWidth: true
      Layout.fillHeight: true
    }
    RowLayout
    {
      RoundButton
      {
        id: _creation_done_
        width: 64
        height: 64
        icon.source: "qrc:/data/done.png"
        function __compute_details(widgets, values)
        {
          if(widgets.length != values.length)
          {
            console.log(widgets, values, " don't have the same length")
          }
          var r = 0
          for(var i = 0; i < Math.min(widgets.length, values.length); ++i)
          {
            if(widgets[i].checked)
            {
              r = r | values[i]
            }
          }
          return r
        }
        onClicked: {
          var brood_details = HiveTracker.broodFlags(__compute_details([_eggs_, _larvae_, _capped_brood_], [HTDB.Inspection.BroodFlags.Eggs, HTDB.Inspection.BroodFlags.Larva, HTDB.Inspection.BroodFlags.CappedBrood]))
          var behavior_details = HiveTracker.behaviorFlags(__compute_details([_aggressive_, _swarming_, _weak_], [HTDB.Inspection.BehaviorFlags.Aggressive, HTDB.Inspection.BehaviorFlags.Swarming, HTDB.Inspection.BehaviorFlags.Weak]))
          var health_details = HiveTracker.healthFlags(__compute_details(
                [_other_, _queenless_, _dead_colony_, _varroa_, _hive_beetles_, _funky_smell_, _bald_brood_, _wax_moths_, _deformed_wings_, _nosema_, _american_fool_brood_, _european_fool_brood_],
                [HTDB.Inspection.HealthFlags.Other, HTDB.Inspection.HealthFlags.Queenless, HTDB.Inspection.HealthFlags.DeadColony, HTDB.Inspection.HealthFlags.Varroa, HTDB.Inspection.HealthFlags.HiveBeetles, HTDB.Inspection.HealthFlags.FunkySmell, HTDB.Inspection.HealthFlags.BaldBrood, HTDB.Inspection.HealthFlags.WaxMoths, HTDB.Inspection.HealthFlags.DeformedWings, HTDB.Inspection.HealthFlags.Nosema, HTDB.Inspection.HealthFlags.AmericanFoolBrood, HTDB.Inspection.HealthFlags.EuropeanFoolBrood ]))
          if(_root_.inspection)
          {
            _root_.inspection.brood = _brood_slider_.value
            _root_.inspection.broodDetails = brood_details
            _root_.inspection.food = _food_slider_.value
            _root_.inspection.behavior = _behavior_slider_.value
            _root_.inspection.behavior_details = behavior_details
            _root_.inspection.health = _health_slider_.value
            _root_.inspection.healthDetails = health_details
            _root_.inspection.comments = _comments_.text
            _root_.inspection.record()
          } else {
            _root_.database.createInspection(
              HiveTracker.createUuid(),
              HiveTracker.hive(_root_.hive),
              _date_time_field_.date,
              _brood_slider_.value,
              brood_details,
              _food_slider_.value,
              _behavior_slider_.value,
              behavior_details,
              _health_slider_.value,
              health_details,
              _comments_.text)
          }
          _root_.StackView.view.pop()
        }
        Material.background: Material.Orange
      }
      RoundButton
      {
        id: _creation_cancel_
        width: 64
        height: 64
        icon.source: "qrc:/data/cancel.png"
        onClicked: _root_.StackView.view.pop()
        Material.background: Material.Red
      }
      Layout.alignment: Qt.AlignRight
      Layout.columnSpan: 2
    }
  }
}
