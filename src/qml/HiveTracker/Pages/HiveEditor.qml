import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

import HiveTracker 1.0
import HiveTrackerDB 1.0 as HTDB

Item
{
  id: _root_
  property HTDB.Database database
  property var apiary
  property var hive
  onHiveChanged:
  {
    if(hive)
    {
      _name_tf_.text = hive.name
    } else {
      _name_tf_.text = ""
    }
  }
  GridLayout
  {
    id: _grid_layout_
    anchors.fill: parent
    columns: 3
    Label
    {
      text: "Name:"
    }
    TextField
    {
      id: _name_tf_
      Layout.columnSpan: 2
      Layout.fillWidth: true
    }
    Item {} // skip
    RowLayout
    {
      RoundButton
      {
        id: _creation_done_
        width: 64
        height: 64
        icon.source: "qrc:/data/done.png"
        enabled: _name_tf_.text.length > 0
        onClicked: {
          if(_root_.hive)
          {
            _root_.hive.name = _name_tf_.text,
            _root_.hive.record()
          } else {
            _root_.database.createHive(
              HiveTracker.createUuid(),
              HiveTracker.apiary(_root_.apiary),
              _name_tf_.text, "")
          }
          _root_.StackView.view.pop()
        }
        Material.background: Material.Orange
      }
      RoundButton
      {
        id: _creation_cancel_
        width: 64
        height: 64
        icon.source: "qrc:/data/cancel.png"
        onClicked: _root_.StackView.view.pop()
        Material.background: Material.Red
      }
      Layout.alignment: Qt.AlignRight
    }
    Item
    {
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.columnSpan: 3
    }
  }
}
