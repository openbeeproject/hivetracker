import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtLocation 5.15
import QtPositioning 5.15

import HiveTracker 1.0
import HiveTrackerDB 1.0 as HTDB

Item
{
  id: _root_
  property HTDB.Database database
  property var apiary
  onApiaryChanged:
  {
    if(apiary)
    {
      _name_tf_.text = apiary.name
      _address_tf_.text = apiary.address
      _longitude_tf_.setLongitude(apiary.longitude)
      _latitude_tf_.setLatitude(apiary.latitude)
    } else {
      _name_tf_.text = ""
      _address_tf_.text = ""
      _longitude_tf_.text = ""
      _latitude_tf_.text = ""
    }
  }
  GridLayout
  {
    id: _grid_layout_
    anchors.fill: parent
    columns: 3
    Label
    {
      text: "Name:"
    }
    TextField
    {
      id: _name_tf_
      Layout.columnSpan: 2
      Layout.fillWidth: true
    }
    Label
    {
      text: "Address:"
    }
    TextArea
    {
      id: _address_tf_
      Layout.columnSpan: 2
      Layout.fillWidth: true
    }
    Label
    {
      text: "Longitude:"
    }
    TextField
    {
      id: _longitude_tf_
      property real longitude: Number.fromLocaleString(Qt.locale(), text)
      Layout.fillWidth: true
      function setLongitude(longitude)
      {
        text = Number(longitude).toLocaleString(Qt.locale(), 'f', 10)
      }
    }
    Button
    {
      Layout.rowSpan: 2
      icon.source: "qrc:/data/map.png"
      onClicked:
      {
        _map_.center = QtPositioning.coordinate(_latitude_tf_.latitude, _longitude_tf_.longitude)
        _map_.visible = true
      }
      Material.background: Material.Yellow
      Layout.preferredHeight: width
    }
    Label
    {
      text: "Latitude:"
    }
    TextField
    {
      id: _latitude_tf_
      property real latitude: Number.fromLocaleString(Qt.locale(), text)
      Layout.fillWidth: true
      function setLatitude(latitude)
      {
        text = Number(latitude).toLocaleString(Qt.locale(), 'f', 10)
      }
    }
    Item {} // skip
    RowLayout
    {
      RoundButton
      {
        id: _creation_done_
        width: 64
        height: 64
        icon.source: "qrc:/data/done.png"
        enabled: _name_tf_.text.length > 0
        onClicked: {
          if(_root_.apiary)
          {
            _root_.apiary.name = _name_tf_.text,
            _root_.apiary.address = _address_tf_.text,
            _root_.apiary.longitude = _longitude_tf_.longitude,
            _root_.apiary.latitude = _latitude_tf_.latitude
            _root_.apiary.record()
          } else {
            _root_.database.createApiary(
              HiveTracker.createUuid(),
              _name_tf_.text,
              _address_tf_.text,
              _longitude_tf_.longitude,
              _latitude_tf_.latitude)
          }
          _root_.StackView.view.pop()
        }
        Material.background: Material.Orange
      }
      RoundButton
      {
        id: _creation_cancel_
        width: 64
        height: 64
        icon.source: "qrc:/data/cancel.png"
        onClicked: _root_.StackView.view.pop()
        Material.background: Material.Red
      }
      Layout.alignment: Qt.AlignRight
    }
    Item
    {
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.columnSpan: 3
    }
  }
  Plugin {
    id: _mapPlugin_
    name: "mapboxgl" // TODO replace with osm when using qt>5.15.11
  }

  Map {
    id: _map_

    visible: false
    anchors.fill: parent
    plugin: _mapPlugin_
    zoomLevel: 14
    MapQuickItem
    {
      coordinate: _map_.center
      sourceItem: Image
      {
        x: -16
        y: -16
        width: 32
        height: 32
        source: "qrc:/data/apiary.png"
      }
    }
    RoundButton
    {
      id: _map_done_
      anchors.right: _map_cancel_.left
      width: 64
      height: 64
      icon.source: "qrc:/data/done.png"
      onClicked: {
        _longitude_tf_.setLongitude(_map_.center.longitude)
        _latitude_tf_.setLatitude(_map_.center.latitude)
        _map_.visible = false
      }
      Material.background: Material.Orange
    }
    RoundButton
    {
      id: _map_cancel_
      anchors.right: parent.right
      width: 64
      height: 64
      icon.source: "qrc:/data/cancel.png"
      onClicked: _map_.visible = false
      Material.background: Material.Red
    }
  }

}
