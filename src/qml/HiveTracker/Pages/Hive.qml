import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15

import HiveTracker 1.0
import HiveTracker.Controls 1.0
import HiveTrackerDB 1.0 as HTDB
import HiveTrackerDB.HiveActivity 1.0 as HTDBHiveActivity

ColumnLayout
{
  id: _root_
  property HTDB.Database database
  property var hive

  HiveHeader
  {
    hive: _root_.hive
    onCloseView: _root_.StackView.view.pop()
  }
  GridLayout
  {
    id: _button_layout_
    columns: 3
    property int __icon_size: 64
    Button
    {
      icon.color: "transparent"
      icon.source: "qrc:/data/magnifier.png"
      icon.width: _button_layout_.__icon_size
      icon.height: _button_layout_.__icon_size
      onClicked:
      {
        _root_.StackView.view.push("InspectionEditor.qml", {"hive": _root_.hive, "database": _root_.database})
      }
      Material.background: Material.Yellow
      Layout.preferredHeight: width
    }
    Button
    {
      icon.color: "transparent"
      icon.source: "qrc:/data/sugar.png"
      icon.width: _button_layout_.__icon_size
      icon.height: _button_layout_.__icon_size
      Material.background: Material.Yellow
      Layout.preferredHeight: width
    }
    Button
    {
      icon.color: "transparent"
      icon.source: "qrc:/data/sugar_magnifier.png"
      icon.width: _button_layout_.__icon_size
      icon.height: _button_layout_.__icon_size
      Material.background: Material.Yellow
      Layout.preferredHeight: width
    }
    Button
    {
      icon.color: "transparent"
      icon.source: "qrc:/data/medication.png"
      icon.width: _button_layout_.__icon_size
      icon.height: _button_layout_.__icon_size
      Material.background: Material.Yellow
      Layout.preferredHeight: width
    }
    Button
    {
      icon.color: "transparent"
      icon.source: "qrc:/data/queen.png"
      icon.width: _button_layout_.__icon_size
      icon.height: _button_layout_.__icon_size
      Material.background: Material.Yellow
      Layout.preferredHeight: width
    }
    Button
    {
      icon.source: "qrc:/data/edit.png"
      icon.width: _button_layout_.__icon_size
      icon.height: _button_layout_.__icon_size
      Material.background: Material.Yellow
      Layout.preferredHeight: width
    }
    Layout.alignment: Qt.AlignHCenter
  }
  HTDBHiveActivity.SelectQuery
  {
    id: _select_hive_activies_
    database: _root_.database
      autoExecute: true
    HTDBHiveActivity.ByHive
    {
      hive: _root_.hive ? _root_.hive.id : 0
    }
    HTDBHiveActivity.OrderByTimestamp
    {
      order: HTDBHiveActivity.OrderByTimestamp.Desc
    }
  }
  ListView
  {
    id: _list_view_
    model: _select_hive_activies_.model
    property var selectedActivity
    property var selectedNativeActivity
    delegate: Pane
    {
      width: _list_view_.width
      property var nativeValue: HTDB.HiveTrackerDB.ptr(modelData.nativeVariant)
      RowLayout
      {
        id: __activity_item_layout__
        width: parent.width
        Image
        {
          source: __sources[modelData.entryClass]
          property var __sources: {
            0: "qrc:/data/magnifier.png",
            1: "qrc:/data/sugar.png",
            2: "qrc:/data/sugar_magnifier.png",
            3: "qrc:/data/medication.png"
          }
          Layout.preferredHeight: label.height
          Layout.preferredWidth: label.height
        }
        Label
        {
          id: label
          text: modelData.timestamp.toLocaleString(undefined, {timeStyle: "short", dateStyle: "short"})
        }
        function __color(v)
        {
          if(v == -1)
          {
            return "gray"
          } else if(v < 5)
          {
            return Qt.rgba(1.0, v/5.0, 0)
          } else {
            return Qt.rgba((10-v)/5.0, 0.5 + (10-v)/5.0, 0)
          }
        }
        Repeater
        {
          model: modelData.entryClass == HTDB.HiveActivity.EntryClass.Inspection ? [nativeValue.brood, nativeValue.food, nativeValue.behavior, nativeValue.health] : 0
          Rectangle
          {
            radius: 0.5*width
            color: __activity_item_layout__.__color(modelData)
            Layout.preferredHeight: label.height
            Layout.preferredWidth: label.height
          }
        }
      }
      MouseArea
      {
        anchors.fill: parent
        onClicked:
        {
          if(_list_view_.selectedActivity == modelData)
          {
            if(modelData.entryClass == HTDB.HiveActivity.EntryClass.Inspection)
            {
              _root_.StackView.view.push("InspectionEditor.qml", {"database": _root_.database, "hive": _root_.selectedHive, "inspection": nativeValue, "editable": false})
            }
          }
          _list_view_.selectedActivity = modelData
          _list_view_.selectedNativeActivity = nativeValue
        }
      }
      contentHeight: __activity_item_layout__.height
      contentWidth: _list_view_.width
      Material.background: _list_view_.selectedActivity == modelData ? Material.Yellow : Material.White
    }
    RowLayout
    {
      ButtonWithConfirmation
      {
        id: _hive_delete_
        icon.source: "qrc:/data/delete.png"
        enabled: _list_view_.selectedNativeActivity != null
        onConfirmed: {
          _list_view_.selectedNativeActivity.erase()
          _list_view_.selectedNativeActivity = null
        }
        Material.background: Material.Red
      }
      anchors.right: parent.right
      anchors.bottom: parent.bottom
    }
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
