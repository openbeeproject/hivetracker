import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import HiveTracker.Pages 1.0
import HiveTrackerDB 1.0 as HTDB

StackView
{
  id: _root_
  property HTDB.Database database
  initialItem: ApiariesList
    {
      id: _apiaries_list_
      database: _root_.database
    }
}
