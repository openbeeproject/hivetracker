import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15

import HiveTrackerDB 1.0 as HTDB
import HiveTracker.Views 1.0

ApplicationWindow
{
  id: _root_
  property HTDB.Database database: HTDBINSTANCE
  width: 300
  height: 600
  visible: true
  header: TabBar {
    id: _bar_
    currentIndex: _swipe_view_.currentIndex
    TabButton {
      text: qsTr("Home")
    }
    TabButton {
      text: qsTr("Apiaries")
      icon.color: "transparent"
      icon.source: "qrc:/data/apiary.png"
    }
    TabButton {
      text: qsTr("Settings")
    }
    Material.accent: Material.Brown
    Material.background: Material.Orange
  }
  SwipeView
  {
    id: _swipe_view_
    currentIndex: _bar_.currentIndex
    anchors.fill: parent
    interactive: false
    Home
    {
      id: _home_
    }
    Apiaries
    {
      id: _apiaries_
      database: _root_.database
    }
    Settings
    {
      id: _settings_
    }
  }
  footer: Pane
  {
    height: _bar_.height
    Material.background: Material.Orange
  }
}
