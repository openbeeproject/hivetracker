#include "Singleton.h"

#include <QUuid>

#include <HiveTrackerDB/Apiary.h>
#include <HiveTrackerDB/Hive.h>
#include <HiveTrackerDB/Inspection.h>

using namespace HiveTracker;

Singleton::Singleton(QObject* _parent) : QObject(_parent)
{
}

Singleton::~Singleton()
{}

QByteArray Singleton::createUuid()
{
  return QUuid::createUuid().toRfc4122();
}

HiveTrackerDB::Apiary Singleton::apiary(HiveTrackerDB::Apiary* _apiary)
{
  return *_apiary;
}

HiveTrackerDB::Apiary* Singleton::apiaryPtr(const HiveTrackerDB::Apiary& _apiary)
{
  return new HiveTrackerDB::Apiary(_apiary);
}

HiveTrackerDB::Hive Singleton::hive(HiveTrackerDB::Hive* _hive)
{
  return *_hive;
}

HiveTrackerDB::Hive* Singleton::hivePtr(const HiveTrackerDB::Hive& _hive)
{
  return new HiveTrackerDB::Hive(_hive);
}

HiveTrackerDB::Inspection* Singleton::inspectionPtr(const HiveTrackerDB::Inspection& _inspection)
{
  return new HiveTrackerDB::Inspection(_inspection);
}

::HiveTrackerDB::Inspection::BroodFlags Singleton::broodFlags(int _value)
{
  return ::HiveTrackerDB::Inspection::BroodFlags(_value);
}

::HiveTrackerDB::Inspection::BehaviorFlags Singleton::behaviorFlags(int _value)
{
  return ::HiveTrackerDB::Inspection::BehaviorFlags(_value);
}

::HiveTrackerDB::Inspection::HealthFlags Singleton::healthFlags(int _value)
{
  return ::HiveTrackerDB::Inspection::HealthFlags(_value);

}

#include "moc_Singleton.cpp"
